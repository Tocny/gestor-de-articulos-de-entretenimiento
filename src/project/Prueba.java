
package project;

import java.util.Scanner;
import project.colors.Colors;
import project.files.MetodosGet;
import project.directorios.*;

public class Prueba {

    /**
     * Atributos de la clase, se usarán para manejar entradas del usuario
     */
    public static MetodosGet entrada = new MetodosGet();
    public static int opcion;

   
    /**
     * Método main
     * @param args[]
     */
    public static void main(String[] args){

        //Presentación
        Colors.println("Hola, este es un gestor de articulos de entretenimiento", Colors.BLUE + Colors.HIGH_INTENSITY);
        StringBuilder sb = new StringBuilder();
        sb.append(Colors.HIGH_INTENSITY);
        sb.append("1. Gestionar Libros.\n");
        sb.append("2. Gestionar Discos.\n");
        sb.append("3. Gestionar Películas.\n");
        sb.append("0. Salir.\n");
        sb.append(Colors.RESTORE);

        do {

            opcion = entrada.getInt(sb.toString(), "Por favor, ingrese un valor válido", 0, 3);

            switch (opcion){

                case 1:
                    DirLibros libros = new DirLibros();
                    libros.menu();
                    break;
            
                case 2:
                    DirDiscos discos = new DirDiscos();
                    discos.menu();
                    break;
            
                case 3:
                    DirPeliculas peliculas = new DirPeliculas();
                    peliculas.menu();
                    break;

            }

        }while(opcion != 0);

        Colors.println("Vuelve pronto :)", Colors.MAGENTA + Colors.HIGH_INTENSITY);

    }
}
