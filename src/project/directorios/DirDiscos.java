package project.directorios;

import project.articulos.Disco;
import project.files.Arreglos  ;
import project.files.MetodosGet;
import project.colors.Colors;
import project.excepciones.*;

/**
 * Clase DirDiscos, para manejar el directorio destinado para objetos de tipo Disco
 */
public class DirDiscos extends Directorio{

    /**
     * Atributos de la clase
     */
    Disco[] discos = null; //Arreglo de objetos de tipo Disco
    Arreglos array = new Arreglos(); //Objeto de tipo Arrays
    MetodosGet entrada = new MetodosGet();//Objeto de tipo Getter
    int opcion;
    String usuario;

    /** 
     * Constructor de la clase, cada que se instancia DirDiscos se ejecuta el método getArray para obtener el arreglo de Discos
    */
    public DirDiscos(){
        getArray();
    }

    /**
     * Método getArray, toma el arreglo de libros de la clase Arrays.
     * Además, ejecuta el método "excepciones", que verifica todas las posibles excepciones que podría tener el arreglo leido
     */
    @Override
    public void getArray(){
        discos = array.arrayDiscos();
        try{
            if(discos == null){
                throw new NotFoundedException();
            }
            excepciones();
        } catch (NotFoundedException e){   
        }
    }

    /**
     * Método excepciones, se encarga de verificar que los elementos del arreglo estén completos y no se repitan titulos
     * de no ser este el caso, se le notifica al usuario los errores al leer el archivo, así como los datos faltantes en este.
     */
    @Override
    public void excepciones(){

        try{
            if(discos.length == 0){//Excepción si recibimos un arreglo con 0 localidades
                throw new IllegalSizeException("Aviso: Aún no tienes elementos guardados");
            
            } else{
                
                //Se verifica que el límite de articulos no se haya alcanzado
                try{

                    if(discos.length >= 32){
                        throw new IllegalSizeException("Aviso: Se ha alcanzado el límite de artículos :o");
                    }

                }catch(IllegalSizeException e){
                    Colors.println(e.getMessage(), Colors.RED + Colors.HIGH_INTENSITY);
                }
                
                //Se verifica que todos los elementos del arreglo estén completos
                for(int i = 0; i < discos.length; i++){
                    try{
                        if(discos[i].getTitulo().length() == 0){
                            throw new EmptySpaceException("Aviso: No se encontró título para el elemento indexado [" + (i+1) + "]");
                        }
                        if(discos[i].getArtista().length() == 0){
                            throw new EmptySpaceException("Aviso: No se encontró interprete para el elemento indexado [" + (i+1) + "]");
                        }
                        if(discos[i].getPistas() == 0){
                            throw new EmptySpaceException("Aviso: No se encontró el número de pistas para el elemento indexado [" + (i+1) + "]");
                        }
                        if(discos[i].getGenero().length() == 0){
                            throw new EmptySpaceException("Aviso: No se encontró genero para el elemento indexado [" + (i+1) + "]");
                        }
                    } catch (EmptySpaceException e){
                        Colors.println(e.getMessage(), Colors.RED + Colors.HIGH_INTENSITY);
                    }
                }
            }
        
            //Por aquí estaría la excepción en caso de que haya titulos repetidos
            String[] titulos = titulos();
            String[] repetidos = new String[titulos.length];
            int nRepetidos = 0;

            for (int i = 0; i < titulos.length; i++) {
                //Si ya está en el arreglo de repetidos, solo se pasa a la siguiente iteración
                if (super.indexado(repetidos, titulos[i])) {
                continue;
                }

                //For para verificar que el título esté repetido para indexarlo o no al arreglo de repetidos
                for (int j = i + 1; j < titulos.length; j++) {
                    //Condición: Si el título no está en repetidos y tiene algúna coincidencia con un título en el arreglo, se agrega al arreglo de repetidos
                    if ((super.indexado(repetidos, titulos[j])==false) && titulos[i].equals(titulos[j])) {
                        repetidos[nRepetidos++] = titulos[j];
                    }
                }
            }

            //Excepción si hay títulos repetidos
            if (nRepetidos > 0) {
                try {
                    throw new RepeatedTitleException("Títulos repetidos encontrados: ");//Simplemente se lanza la excepción
                } catch (RepeatedTitleException e) {
                    //Se imprime el mensaje con todos los títulos repetidos
                    Colors.println(e.getMessage(), Colors.RED + Colors.HIGH_INTENSITY);
                    for(int i = 0; i < nRepetidos; i++){
                        Colors.println(repetidos[i], Colors.RED + Colors.HIGH_INTENSITY);
                    }
                }
            } 

        }catch (IllegalSizeException e){
            Colors.println(e.getMessage(), Colors.RED + Colors.HIGH_INTENSITY);
        }
    }

    /**
     * Método títulos, genera un arreglo con los títulos en arreglo de discos
     * @return titulos el arreglo del atributo título
     */
    public String[] titulos(){
        String[] titulos = new String[discos.length];
        for(int i = 0; i < discos.length; i++){
            titulos[i] = discos[i].getTitulo();
        }
        return titulos;
    }


    /**
     * Método consultaCompleta, para generar el listado general de los Discos en el arreglo
     */
    @Override
    public void consultaCompleta(){
        //Verificación para considerar que el arreglo podría estar vacío
        try{
            if(discos.length == 0){
                throw new IllegalSizeException("Aún no hay elementos guardados");
            } else {
                //Ciclo para imprimir todos los sb del arreglo
                for(int i = 0; i < discos.length; i++){
                System.out.println(discos[i].toString());
                }
            }
        } catch (IllegalSizeException e){
            Colors.println(e.getMessage(), Colors.CYAN + Colors.HIGH_INTENSITY);
        }

    }

    /**
     * Método "consultaArtista", busca objetos de tipo Disco en el arreglo según el atributo "artista"
     */
    @Override
    public void consultaArtista(){
        
        usuario = entrada.getString("Ingrese el/la interprete a buscar", "Por favor, proporcione una entrada válida");
        int coincidencias = 0;

        //Chequeo de la existencia de coincidencias, de otro modo se arroja una excepción
        try{
            for(int i = 0; i < discos.length; i++){    
                if(usuario.equals(discos[i].getArtista())){
                    System.out.println(discos[i].toString());
                    coincidencias++;
                }
            }
            if(coincidencias == 0){
                throw new NotFoundedException("No se encontró el/la interprete");
            }

        } catch(NotFoundedException e){
            Colors.println(e.getMessage(), Colors.RED + Colors.HIGH_INTENSITY);
        }
    }

    /**
     * Método "consultaGenero", se buscan objetos de tipo Disco según el atributo "genero" y se imprime el interprete
     */
    @Override
    public void consultaGenero(){
        usuario = entrada.getString("Ingrese el genero", "Por favor, proporcione una entrada válida");
        int coincidencias = 0;
        
        Colors.println("Interpretes del género \"" + usuario + "\"", Colors.MAGENTA + Colors.HIGH_INTENSITY);
        //Chequeo de la existencia de coincidencias, de otro modo se arroja una excepción
        try{
            
            for(int i = 0; i < discos.length; i++){  
                if(usuario.equals(discos[i].getGenero())){
                    Colors.println("❒ " + discos[i].getArtista(), Colors.MAGENTA);
                    coincidencias++;
                }
            }
            if(coincidencias != 0){
                System.out.println();
            } else if(coincidencias == 0){
                throw new NotFoundedException("No se encontraron interpretes del género \"" + usuario + "\"");
            }

        } catch(NotFoundedException e){
            Colors.println(e.getMessage() + "\n", Colors.RED + Colors.HIGH_INTENSITY);
        }
    }

    /**
     * Método consultaDiezPistas, se encarga de buscar objetos de tipo Disco según el valor del atributo "pistas"
     */
    public void consultaDiezPistas(){
        int coincidencias = 0;
        
        //Chequeo de la existencia de coincidencias en el arreglo, de otro modo se arroja una excepción
        try{
            for(int i = 0; i < discos.length; i++){  
                    if(discos[i].getPistas() >= 10){  
                        System.out.println(discos[i].toString());
                        coincidencias++;
                    }   
            }
            if(coincidencias == 0){
                throw new NotFoundedException("No hay discos con mas de 10 pistas");
            }

        } catch(NotFoundedException e){
            Colors.println(e.getMessage(), Colors.CYAN + Colors.HIGH_INTENSITY);
        }
    }

    /**
     * Método agregar, se encarga de recibir las entradas del usuario en los archivos
     * se apoya del método heredado, "write"
    */
   @Override
    public void agregar(){
        String escribir,titulo;
        String error = "Proporcione una entrada válida";
        
        //Chequeo de que no se haya superado aún el límite de almacenamiento
        try{
            if(discos.length<32){
                
                do{
                titulo = entrada.getString("Ingrese el nombre del disco: ", error);
                }while(super.isRepetido(titulo, titulos()));

                String genero = entrada.getString("Ingrese el género: ", error);
                String interprete = entrada.getString("Ingrese el interprete: ", error);
                int pistas = entrada.getInt("Ingrese el número de pistas", error, 1, Integer.MAX_VALUE);
        
                escribir = titulo + "," + genero + "," + interprete + "," + pistas;
                super.write(escribir, "discos.csv");
                Colors.println("Se agregó " + "\"" + titulo + "\"" + " correctamente \n", Colors.BLUE);
                getArray();
            } else{
                throw new IllegalSizeException("Límite de articulos alcanzado :(");
            }
        } catch (IllegalSizeException e){
                    Colors.println(e.getMessage(), Colors.RED + Colors.HIGH_INTENSITY);
            }
    }

    /**
     * Método toString para generar el menú de las funciones para los Discos
     * @return String, la cadena del menú completa
     */
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
            sb.append(Colors.HIGH_INTENSITY);
            sb.append("1. Consulta por interprete.\n");
            sb.append("2. Consulta interpretes por genero.\n");
            sb.append("3. Consulta por más de 10 pistas.\n");
            sb.append("4. Consulta completa.\n");
            sb.append("5. Añadir disco.\n");
            sb.append("0. Regresar al menú anterior.\n");
            sb.append(Colors.RESTORE);
            return sb.toString();
    }

    /**
     * Método "menu", ejecuta las funciones destinadas para el arreglo en un bucle para simular el menú e invocar los métodos
     */
    @Override
    public void menu(){
        try{

            if(discos == null){
                throw new NotFoundedException("Regresando al menú principal");
            }
        
            do {
                //Entrada del usuario
                opcion = entrada.getInt(toString(), "Por favor, ingrese un valor válido", 0, 5);

                switch (opcion){

                    case 1:
                        consultaArtista();
                        break;
            
                    case 2:
                        consultaGenero();
                        break;
            
                    case 3:
                        consultaDiezPistas();
                        break;

                    case 4:
                        consultaCompleta();
                        break;
                
                    case 5:
                        agregar();
                        break;
                }
            }while(opcion != 0);

        }catch (NotFoundedException e){
            Colors.println(e.getMessage(), Colors.CYAN + Colors.HIGH_INTENSITY);
        }
    }
}
