package project.directorios;

/**
 * Importación de paquetes requeridos
 */
import project.files.Arreglos;
import project.colors.Colors;
import project.files.ReaderWriter;
import project.excepciones.*;

/**
 * Clase abstracta Directorio
 * está destinada a manejar la jerarquía de herencia para los métodos
 * abstractos ubicados en cada directorio según el arreglo que se utilice.
 */
public abstract class Directorio{
    /**
     * Atributo de tipo ReaderWriter, para escribir sobre un archivo en especifico
     */
    ReaderWriter readWrite = new ReaderWriter();

    /**
     * Método abstracto getArray, se utilizará polimorfismo para usar un arreglo distinto en cada caso
     */
    public abstract void getArray();

    /**
     * Método abstracto excepciones, se utilizará polimorfismo para usar un arreglo distinto en cada caso.
     * Su tarea es verificar las posibles excepciones al momento de generar el arreglo de artículos
     */
    public abstract void excepciones();

    /**
     * Método abstractro consulta Completa, se usará polimorfismo para revisar un arreglo diferente seǵun sea el caso
     */
    public abstract void consultaCompleta();

    /**
     * Método abstracto consultaArtista, para manejar los artistas de cada arreglo
     */
    public abstract void consultaArtista();

    /**
     * Método abstracto consultaGenero, para manejar los generos según cada arreglo
     */
    public abstract void consultaGenero();

    /**
     * Método abstracto menu, para generar las acciones generales de cada Directorio
     */
    public abstract void agregar();

    /**
     * Método abstracto menu, para generar las acciones generales de cada Directorio
     */
    public abstract void menu();

    /**
     * Método write, para escribir en un archivo dados una cadena y el nombre del archivo
     * @param cadena cadena recibida
     * @param archivo nombre del archivo
     */
    public void write(String cadena, String archivo){
        readWrite.write(cadena, archivo);
    }

    /**
     * Método indexado, devuelve un booleano en caso de que una cadena ya esté en un arreglo de cadenas
     * @param array arreglo de Strings
     * @param text la cadena que se dese verificar
     * @return booleano según se encuentre o no en el arreglo
     */
    public boolean indexado(String[] array, String text) {
    for (int i = 0; i < array.length; i++) {
        // Verifica si el elemento en la posición i es null antes de compararlo
        if (array[i] != null && array[i].equals(text)) {
            return true;
        }
    }
    return false;
    }

    /**
     * Método isRepetido, se encarga de revisar si un titulo está repetido
     * Se usa exclusivamente para manejar una excepción
     * @param title titulo que se desea verificar
     * @param titulos arreglo de Strings
     * @return booleano según se encuentre o no en el arreglo
     * 
     */
    public boolean isRepetido(String title, String[] titulos){
        
        try{
        for(int i = 0; i < titulos.length; i++){
            if(titulos[i].equals(title)){
                throw new RepeatedTitleException("Título repetido: " + title);
            }
        }
        return false;
        } catch (RepeatedTitleException e){
                Colors.println(e.getMessage(), Colors.RED + Colors.HIGH_INTENSITY);
                return true;
            }
    }

}