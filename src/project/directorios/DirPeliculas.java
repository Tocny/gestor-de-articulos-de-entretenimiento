package project.directorios;

import project.articulos.Pelicula;
import project.files.Arreglos  ;
import project.files.MetodosGet;
import project.colors.Colors;
import project.excepciones.*;
import java.util.Arrays;

/**
 * Clase DirPeliculas, para manejar el directorio destinado para objetos de tipo Pelicula
 */
public class DirPeliculas extends Directorio{

    /**
     * Atributos de la clase
     */
    Pelicula[] peliculas = null; //Arreglo de objetos de tipo Pelicula
    Arreglos array = new Arreglos(); //Objeto de tipo Arrays
    MetodosGet entrada = new MetodosGet();//Objeto de tipo Getter
    int opcion;
    String usuario;

    /** 
     * Constructor de la clase, cada que se instancia FuncionPelicula se ejecuta el método getArray para obtener el arreglo de Peliculas
    */
    public DirPeliculas(){
        getArray();
    }

    /**
     * Método getArray, toma el arreglo de peliculas de la clase Arrays.
     */
    @Override
    public void getArray(){
        peliculas = array.arrayPeliculas();
        try{
            if(peliculas == null){
                throw new NotFoundedException();
            }
            excepciones();
        } catch (NotFoundedException e){   
        }
    }

    /**
     * Método excepciones, se encarga de verificar que los elementos del arreglo estén completos y no se repitan titulos
     * de no ser este el caso, se le notifica al usuario los errores al leer el archivo, así como los datos faltantes en este
     */
    @Override
    public void excepciones(){

        try{
            if(peliculas.length == 0){//Excepción si recibimos un arreglo con 0 localidades
                throw new IllegalSizeException("Aviso: Aún no tienes elementos guardados");
            
            } else{

                //Se verifica que el límite de articulos no se haya alcanzado
                try{
                    if(peliculas.length >= 32){
                        throw new IllegalSizeException("Aviso: Se ha alcanzado el límite de artículos :o");
                    }
                }catch(IllegalSizeException e){
                    Colors.println(e.getMessage(), Colors.RED + Colors.HIGH_INTENSITY);
                }

                //Se verific que todos los elementos del arreglo estén completos
                for(int i = 0; i < peliculas.length; i++){
                    try{
                        if(peliculas[i].getTitulo().length() == 0){
                            throw new EmptySpaceException("Aviso: No se encontró título para el elemento indexado [" + (i+1) + "]");
                        }
                        if(peliculas[i].getArtista().length() == 0){
                            throw new EmptySpaceException("Aviso: No se encontró actor/actriz para el elemento indexado [" + (i+1) + "]");
                        }
                        if(peliculas[i].getAño() == 0){
                            throw new EmptySpaceException("Aviso: No se encontró el año de estreno para el elemento indexado [" + (i+1) + "]");
                        }
                        if(peliculas[i].getGenero().length() == 0){
                            throw new EmptySpaceException("Aviso: No se encontró genero para el elemento indexado [" + (i+1) + "]");
                        }
                    } catch (EmptySpaceException e){
                        Colors.println(e.getMessage(), Colors.RED + Colors.HIGH_INTENSITY);
                    }
                }
            }
        
            //Por aquí estaría la excepción en caso de que haya titulos repetidos
            String[] titulos = titulos();
            String[] repetidos = new String[titulos.length];
            int nRepetidos = 0;

            for (int i = 0; i < titulos.length; i++) {
                //Si ya está en el arreglo de repetidos, solo se pasa a la siguiente iteración
                if (super.indexado(repetidos, titulos[i])) {
                    continue;
                }

                //For para verificar que el título esté repetido para indexarlo o no al arreglo de repetidos
                for (int j = i + 1; j < titulos.length; j++) {
                    //Condición: Si el título no está en repetidos y tiene algúna coincidencia con un título en el arreglo, se agrega al arreglo de repetidos
                    if ((super.indexado(repetidos, titulos[j])==false) && titulos[i].equals(titulos[j])) {
                        repetidos[nRepetidos++] = titulos[j];
                    }
                }
            }

            //Excepción si hay títulos repetidos
            if (nRepetidos > 0) {
                try {
                    throw new RepeatedTitleException("Títulos repetidos encontrados: ");//Simplemente se lanza la excepción
                } catch (RepeatedTitleException e) {
                    //Se imprime el mensaje con todos los títulos repetidos
                    Colors.println(e.getMessage(), Colors.RED + Colors.HIGH_INTENSITY);
                    
                    for(int i = 0; i < nRepetidos; i++){
                    Colors.println(repetidos[i], Colors.RED + Colors.HIGH_INTENSITY);
                    }
                }
            } 

        }catch (IllegalSizeException e){
            Colors.println(e.getMessage(), Colors.RED + Colors.HIGH_INTENSITY);
        }
    }

    /**
     * Método títulos, genera un arreglo con los títulos en arreglo de peliculas
     * @return titulos el arreglo del atributo título
     */
    public String[] titulos(){
        String[] titulos = new String[peliculas.length];
        for(int i = 0; i < peliculas.length; i++){
            titulos[i] = peliculas[i].getTitulo();
        }
        return titulos;
    }


    /**
     * Método consultaCompleta, para generar el listado general de Peliculas
     */
    @Override
    public void consultaCompleta(){
        try{
            if(peliculas.length == 0){
                throw new IllegalSizeException("Aún no hay elementos guardados");
            } else {
                for(int i = 0; i < peliculas.length; i++){
                System.out.println(peliculas[i].toString());
                }
            }
        } catch (IllegalSizeException e){
            Colors.println(e.getMessage(), Colors.CYAN + Colors.HIGH_INTENSITY);
        }

    }

    /**
     * Método consultaArtista, se encarga de buscar la filmografía de un actor o actriz en orden de estreno de cada película
     */
    @Override
    public void consultaArtista(){
        
        //Entrada del usuario
        usuario = entrada.getString("Ingrese el/la actriz a buscar", "Por favor, proporcione una entrada válida");
        //variables y arreglos necesarios para el ordenamiento
        int coincidencias = 0;
        Pelicula[] actor = new Pelicula[peliculas.length];
        short[] años = new short[peliculas.length];
        
        try{
            //Ciclo para generar el arreglo de años y actores
            for(int i = 0; i < peliculas.length; i++){    
                if(usuario.equals(peliculas[i].getArtista())){
                    actor[coincidencias] = peliculas[i];
                    años[coincidencias] = peliculas[i].getAño();
                    coincidencias++;
                }
            }

            if(coincidencias == 0){
                throw new NotFoundedException("No se encontró el/la actriz");
            }

            //Generación de otro arreglo, el cual será una copia del arreglo años, pero con una longitud exacta
            short[] ordenado = new short[coincidencias];
            for(int i = 0; i < peliculas.length; i++){
                if(años[i] != 0){
                ordenado[i] = años[i];
                }
            }
    
            //Instrucción para ordenar de forma ascendente los valores del arreglo "ordenado"
            Arrays.sort(ordenado);
        
            //Impresión de los toStrings para la filmografía ordenada
            for(int j = 0; j < ordenado.length; j++){
                for(int i = 0; i < coincidencias; i++){
                    if(ordenado[j] == actor[i].getAño()){
                        System.out.println(actor[i].toString());
                    }
                }
            }
        
        } catch(NotFoundedException e){
            Colors.println(e.getMessage(), Colors.RED + Colors.HIGH_INTENSITY);
        }
    }

    /**
     * Método "consultaAño", se usará para buscar entre las peliculas según el atributo "año"
     */
    public void consultaAño(){
        int año = entrada.getInt("Ingrese el año: ","Por favor, ingrese un valor válido", 0, Integer.MAX_VALUE);
        int coincidencias = 0;
        //Chequeo de la existencia de coincidencias
        try{
            for(int i = 0; i < peliculas.length; i++){  
                    if(peliculas[i].getAño() == año){  
                        System.out.println(peliculas[i].toString());
                        coincidencias++;
                    }   
            }
            if(coincidencias == 0){
                throw new NotFoundedException("No se encontró dicho año");
            }

        } catch(NotFoundedException e){
            Colors.println(e.getMessage(), Colors.RED + Colors.HIGH_INTENSITY);
        }
    }

    /**
     * Método consultaGenero, busca películas según el atributo "genero"
     */
    @Override
    public void consultaGenero(){
        String genero = entrada.getString("Ingrese el genero: ","Por favor, ingrese un valor válido");
        int coincidencias = 0;
        try{
            for(int i = 0; i < peliculas.length; i++){  
                    if(peliculas[i].getGenero().equals(genero)){  
                        System.out.println(peliculas[i].toString());
                        coincidencias++;
                    }
                
            }

            if(coincidencias == 0){
                throw new NotFoundedException("No se encontró el género");
            }

        } catch(NotFoundedException e){
            Colors.println(e.getMessage(), Colors.RED + Colors.HIGH_INTENSITY);
        }
    }

    /**
     * Método agregar, se encarga de recibir las entradas del usuario en los archivos
     * se apoya del método heredado, "write"
    */
   @Override
    public void agregar(){
        String escribir,titulo;
        String error = "Proporcione una entrada válida";
        try{
            if(peliculas.length<32){
                
                do{
                titulo = entrada.getString("Ingrese el titulo de la película: ", error);
                }while(super.isRepetido(titulo, titulos()));

                String genero = entrada.getString("Ingrese el género: ", error);
                String interprete = entrada.getString("Ingrese el/la actor/actriz pricipal: ", error);
                short año = entrada.getShort("Ingrese el año de estreno: ", error, 1, Short.MAX_VALUE);
        
                escribir = titulo + "," + genero + "," + interprete + "," + año;
                super.write(escribir, "peliculas.csv");
                
                Colors.println("Se agregó " + "\"" + titulo + "\"" + " correctamente \n", Colors.BLUE);
                getArray();

            } else{
                throw new IllegalSizeException("Límite de articulos alcanzado :(");
            }
        } catch (IllegalSizeException e){
                    Colors.println(e.getMessage(), Colors.RED + Colors.HIGH_INTENSITY);
            }
    }

    /**
     * Método toString para generar el menú de las funciones para los Peliculas
     * @return String, la cadena del menú completa
     */
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
            sb.append(Colors.HIGH_INTENSITY);
            sb.append("1. Consultar por año.\n");
            sb.append("2. Consultar filmografía de algún actor/actriz en orden de estreno.\n");
            sb.append("3. Consulta por género.\n");
            sb.append("4. Consulta completa.\n");
            sb.append("5. Añadir pelicula.\n");
            sb.append("0. Regresar al menú anterior.\n");
            sb.append(Colors.RESTORE);
            return sb.toString();
    }

    /**
     * Método "menu", ejecuta las funciones definidas en la clase en un bucle para simular el menú e invocar los métodos
     */
    @Override
    public void menu(){
        try{
            if(peliculas == null){
                throw new NotFoundedException("Regresando al menú principal");
            }

            do {

                opcion = entrada.getInt(toString(), "Por favor, ingrese un valor válido", 0, 5);

                switch (opcion){

                    case 1:
                        consultaAño();
                        break;
            
                    case 2:
                        consultaArtista();
                        break;
            
                    case 3:
                        consultaGenero();
                        break;

                    case 4:
                        consultaCompleta();
                        break;
                
                    case 5:
                        agregar();
                        break;
            }

            }while(opcion != 0);
        } catch (NotFoundedException e){
            Colors.println(e.getMessage(), Colors.CYAN + Colors.HIGH_INTENSITY);
        }
    
    }
}