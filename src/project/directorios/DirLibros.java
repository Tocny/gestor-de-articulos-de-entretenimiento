package project.directorios;

import project.articulos.Libro;
import project.files.Arreglos  ;
import project.files.MetodosGet;
import project.colors.Colors;
import project.excepciones.*;

/**
 * Clase DirLibros, para manejar el directorio destinado para objetos de tipo Libro
 */
public class DirLibros extends Directorio{

    /**
     * Atributos de la clase
     */
    Libro[] libros = null; //Arreglo de objetos de tipo Libro
    Arreglos array = new Arreglos(); //Objeto de tipo Arrays
    MetodosGet entrada = new MetodosGet();//Objeto de tipo Getter
    int opcion;
    String usuario;

    /** 
     * Constructor de la clase, cada que se instancia "DirLibros" se ejecuta el método "getArray" para obtener el arreglo de Libros
    */
    public DirLibros(){
        getArray();
    }

    /**
     * Método getArray, toma el arreglo de libros de la clase Arrays.
     * Además, ejecuta el método "excepciones", que verifica todas las posibles excepciones que podría tener el arreglo leido
     */
    @Override
    public void getArray(){
        libros = array.arrayLibros();
        try{
            if(libros == null){
                throw new NotFoundedException();
            }
            excepciones();
        } catch (NotFoundedException e){   
        }
    }

    /**
     * Método excepciones, se encarga de verificar que los elementos del arreglo estén completos y no se repitan titulos
     * de no ser este el caso, se le notifica al usuario los errores al leer el archivo, así como los datos faltantes en este
     */
    @Override
    public void excepciones(){

        try{
            if(libros.length == 0){//Excepción si recibimos un arreglo con 0 localidades
                throw new IllegalSizeException("Aviso: Aún no tienes elementos guardados");
            
            } else{

                //Se verifica que el límite de articulos no se haya alcanzado
                try{
                    if(libros.length >= 32){
                        throw new IllegalSizeException("Aviso: Se ha alcanzado el límite de artículos :o");
                    }
                }catch(IllegalSizeException e){
                    Colors.println(e.getMessage(), Colors.RED + Colors.HIGH_INTENSITY);
                }

                //Se verifica que todos los elementos del arreglo estén completos
                for(int i = 0; i < libros.length; i++){
                    try{
                        if(libros[i].getTitulo().length() == 0){
                            throw new EmptySpaceException("Aviso: No se encontró título para el elemento indexado [" + (i+1) + "]");
                        }
                        if(libros[i].getArtista().length() == 0){
                            throw new EmptySpaceException("Aviso: No se encontró autor para el elemento indexado [" + (i+1) + "]");
                        }
                        if(libros[i].getTema() == 0){
                            throw new EmptySpaceException("Aviso: No se encontró tema para el elemento indexado [" + (i+1) + "]");
                        }
                        if(libros[i].getGenero().length() == 0){
                            throw new EmptySpaceException("Aviso: No se encontró genero para el elemento indexado [" + (i+1) + "]");
                        }
                    } catch (EmptySpaceException e){
                        Colors.println(e.getMessage(), Colors.RED + Colors.HIGH_INTENSITY);
                    }
                }
            }
        
            //Por aquí estaría la excepción en caso de que haya titulos repetidos
            String[] titulos = titulos();
            String[] repetidos = new String[titulos.length];
            int nRepetidos = 0;

            for (int i = 0; i < titulos.length; i++) {
                //Si ya está en el arreglo de repetidos, solo se pasa a la siguiente iteración
                if (super.indexado(repetidos, titulos[i])) {
                    continue;
                }
            
                //For para verificar que el título que esté repetido para indexarlo o no al arreglo de repetidos
                for (int j = i + 1; j < titulos.length; j++) {
                    //Condición: Si el título no está en repetidos y tiene algúna coincidencia con un título en el arreglo, se agrega al arreglo de repetidos
                    if ((super.indexado(repetidos, titulos[j])==false) && titulos[i].equals(titulos[j])) {
                        repetidos[nRepetidos++] = titulos[j];
                    }
                }
            }

            //Excepción si hay títulos repetidos
            if (nRepetidos > 0) {
                try {
                    throw new RepeatedTitleException("Títulos repetidos encontrados: ");//Simplemente se lanza la excepción
                } catch (RepeatedTitleException e) {
                    //Se imprime el mensaje con todos los títulos repetidos
                    Colors.println(e.getMessage(), Colors.RED + Colors.HIGH_INTENSITY);
                    for(int i = 0; i < nRepetidos; i++){
                        Colors.println(repetidos[i], Colors.RED + Colors.HIGH_INTENSITY);
                    }
                }
            } 

        }catch (IllegalSizeException e){
            Colors.println(e.getMessage(), Colors.RED + Colors.HIGH_INTENSITY);
        }
    }

    /**
     * Método títulos, genera un arreglo con los títulos en arreglo de libros
     * @return titulos el arreglo del atributo título
     */
    public String[] titulos(){
        String[] titulos = new String[libros.length];
        for(int i = 0; i < libros.length; i++){
            titulos[i] = libros[i].getTitulo();
        }
        return titulos;
    }


    /**
     * Método consultaCompleta, para generar el listado general de Libros
     */
    @Override
    public void consultaCompleta(){

        //Chequeo para revisar que la base de datos no esté vacía
        try{
            if(libros.length == 0){
                throw new IllegalSizeException("Aún no hay elementos guardados");
            } else {
                //Ciclo para imprimir todos los sb del arreglo
                for(int i = 0; i < libros.length; i++){
                System.out.println(libros[i].toString());
                }
            }
        } catch (IllegalSizeException e){
            Colors.println(e.getMessage(), Colors.CYAN + Colors.HIGH_INTENSITY);
        }
    }

    /**
     * Método "consultaArtista" para buscar entre los obgetos según el atributo "artista"
     */
    @Override
    public void consultaArtista(){
        
        //Solicitud de datos al usuario
        usuario = entrada.getString("Ingrese el/la autor a buscar", "Por favor, proporcione una entrada válida");
        int coincidencias = 0;
        
        //Chequeo para buscar coincidencias
        try{
            for(int i = 0; i < libros.length; i++){ 

                //Si se encuentran coincidencias se umprime el sb del objeto y se incrementa el contador "coincidencias"   
                if(usuario.equals(libros[i].getArtista())){
                    System.out.println(libros[i].toString());
                    coincidencias++;
                }
            }

            if(coincidencias == 0){
                throw new NotFoundedException("No se encontró el/la autor");
            }

        } catch(NotFoundedException e){
            Colors.println(e.getMessage(), Colors.RED + Colors.HIGH_INTENSITY);
        }
    }

    /**
     * Método "consultaGenero", se usará para buscar entre los libros según el atributo "tema"
     */
    @Override
    public void consultaGenero(){

        //Solicitud de datos al usuario
        int tema = entrada.getInt("Ingrese el tema","Por favor, ingrese un valor válido", 0, Integer.MAX_VALUE);
        int coincidencias = 0;

        //Cequeo de que esté el dato solicitado
        try{
            for(int i = 0; i < libros.length; i++){ 

                    //Si se encuentran coincidencias se imprime el sb del objeto y se incrementa el contador de coincidencias 
                    if(libros[i].getTema() == tema){  
                        System.out.println(libros[i].toString());
                        coincidencias++;
                    }
            }
            
            //En caso de que el contador de coincidencias sea nulo
            if(coincidencias == 0){
                throw new NotFoundedException("No se encontró el tema");
            }

        } catch(NotFoundedException e){
            Colors.println(e.getMessage(), Colors.RED + Colors.HIGH_INTENSITY);
        }
    }

    /**
     * Método "consultaTitulo", se encarga de buscar en el arreglo según el atributo "titulo"
     */
    public void consultaTitulo(){

        //Solicitud de datos al usuario
        usuario = entrada.getString("Ingrese el titulo", "Por favor, proporcione una entrada válida");
        boolean founded = false;

        //Cequeo de que esté el dato solicitado
        try{
            for(int i = 0; i < libros.length; i++){ 

                //Si se encuentra la coincidencia se imprime el sb y se termina el ciclo
                if(usuario.equals(libros[i].getTitulo())){
                    System.out.println(libros[i].toString());
                    founded = true;
                    break;
                }
            }
            //En caso de que no se hayan encontrado coincidencias
            if(founded == false){
                throw new NotFoundedException("No se encontró el libro");
            }

        } catch(NotFoundedException e){
            Colors.println(e.getMessage(), Colors.RED + Colors.HIGH_INTENSITY);
        }
    }

    /**
     * Método agregar, se encarga de recibir las entradas del usuario en los archivos
     * se apoya del método heredado, "write"
    */
   @Override
    public void agregar(){
        String escribir,titulo;
        String error = "Proporcione una entrada válida";
        try{
            if(libros.length<32){//Verificación de no se supere el almacenamiento 

                //Obtención de los datos a manddar
                do{
                titulo = entrada.getString("Ingrese el título del libro: ", error);
                }while(super.isRepetido(titulo, titulos()));

                String genero = entrada.getString("Ingrese el género: ", error);
                String autor = entrada.getString("Ingrese el autor: ", error);
                int tema = entrada.getInt("Ingrese el tema", error, 1, Integer.MAX_VALUE);
                
                //Generación de la cadena a agregar
                escribir = titulo + "," + genero + "," + autor + "," + tema;
                //Llamada al método write
                super.write(escribir, "libros.csv");
                Colors.println("Se agregó " + "\"" + titulo + "\"" + " correctamente \n", Colors.BLUE);
                getArray();//Vuelve a leer el arreglo
            
            } else{//Excepción en caso de que ya se haya superado el límite de articulos
                throw new IllegalSizeException("Límite de articulos alcanzado :(");
            }
        } catch (IllegalSizeException e){
                Colors.println(e.getMessage(), Colors.RED + Colors.HIGH_INTENSITY);
            }
    }

    /**
     * Método toString para generar el menú de las funciones para los Libros
     * @return sb, la cadena del menú completa
     */
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
            sb.append(Colors.HIGH_INTENSITY);
            sb.append("1. Consulta por autor.\n");
            sb.append("2. Consulta por tema.\n");
            sb.append("3. Consulta por título.\n");
            sb.append("4. Consulta completa.\n");
            sb.append("5. Añadir libro.\n");
            sb.append("0. Regresar al menú anterior.\n");
            sb.append(Colors.RESTORE);
            return sb.toString();
    }

    /**
     * Método "menu", ejecuta las funciones destinadas para el arreglo de Libros en un bucle para simular el menú e invocar los métodos.
     */
    @Override
    public void menu(){
        try{
            if(libros == null){
                throw new NotFoundedException("Regresando al menú principal");
            }
            do {
            
                //Obtención de la entrada del usuario
                opcion = entrada.getInt(toString(), "Por favor, ingrese un valor válido", 0, 5);
            
                //Switch para el menú
                switch (opcion){

                    case 1:
                        consultaArtista();
                        break;
            
                    case 2:
                        consultaGenero();
                        break;
            
                    case 3:
                        consultaTitulo();
                        break;

                    case 4:
                        consultaCompleta();
                        break;
                
                    case 5:
                        agregar();
                        break;

            }

        }while(opcion != 0);
        
        } catch(NotFoundedException e){
            Colors.println(e.getMessage(), Colors.CYAN + Colors.HIGH_INTENSITY);
        }
    }
}