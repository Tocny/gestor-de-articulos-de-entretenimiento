package project.excepciones;

/**
 * Excepción EmptySpaceException. Indica espacios donde hay elementos nulos en el archivo a leer.
 */
public class EmptySpaceException extends Exception {

    /**
     * Constructor. En caso de querer indicar la aparición de la excepción
     */
    public EmptySpaceException() {
        super();
    }

    /**
     * Constructor.
     *
     * @param message Informacion detallada sobre el contexto en que ocurre
     * la excepcion.
     */
    public EmptySpaceException(String message){
        super(message);
    }
}