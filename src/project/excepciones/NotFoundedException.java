package project.excepciones;

/**
 * Excepción NonReadedException. Indica que, en un metodo de busqueda, no se encontró el dato deseado.
 */
public class NotFoundedException extends Exception {

    /**
     * Constructor. En caso de querer indicar la aparición de la excepción
     */
    public NotFoundedException() {
        super();
    }

    /**
     * Constructor.
     *
     * @param message Informacion detallada sobre el contexto en que ocurre
     * la excepcion.
     */
    public NotFoundedException(String message){
        super(message);
    }
}