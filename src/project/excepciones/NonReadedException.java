package project.excepciones;

/**
 * Excepción NonReadedException. Indica que hubo un objeto que no fue leido.
 */
public class NonReadedException extends Exception {

    /**
     * Constructor. En caso de querer indicar la aparición de la excepción
     */
    public NonReadedException() {
        super();
    }

    /**
     * Constructor.
     *
     * @param message Informacion detallada sobre el contexto en que ocurre
     * la excepcion.
     */
    public NonReadedException(String message){
        super(message);
    }
}