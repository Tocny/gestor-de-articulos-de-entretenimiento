package project.excepciones;

/**
 * Excepción RepeatedTitleException. Indica que hay un título repetido en la base de datos.
 */
public class RepeatedTitleException extends Exception {

    /**
     * Constructor. En caso de querer indicar la aparición de la excepción
     */
    public RepeatedTitleException() {
        super();
    }

    /**
     * Constructor.
     *
     * @param message Informacion detallada sobre el contexto en que ocurre
     * la excepcion.
     */
    public RepeatedTitleException(String message){
        super(message);
    }
}
