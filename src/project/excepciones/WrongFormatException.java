package project.excepciones;

/**
 * Excepción WrongFormatException. Indica que se encontró un error de formato al leer el archivo.
 */
public class WrongFormatException extends Exception {

    /**
     * Constructor. En caso de querer indicar la aparición de la excepción
     */
    public WrongFormatException() {
        super();
    }

    /**
     * Constructor.
     *
     * @param message Informacion detallada sobre el contexto en que ocurre
     * la excepcion.
     */
    public WrongFormatException(String message){
        super(message);
    }
}