package project.files;

import project.articulos.*;
import project.files.ReaderWriter;
import java.io.IOException;
import project.colors.Colors;
import project.excepciones.*;
import java.lang.NullPointerException;


/**
 * Clase Arreglos, para generar los arreglos de Articulos según se requiera
 */
public class Arreglos{

    /**
     * Método getDiscos, genera el arreglo de objetos de tipo Disco tomado del archivo discos.csv
     * @return arrayDiscos, el arreglo de objetos de tipo Disco
     */
    public Disco[] arrayDiscos() {
        Disco[] arrayDiscos = new Disco[0];

        try{
            //Generación de un arreglo empleando ReaderWriter y leyendo el archivo deseado
            String[] renglones = ReaderWriter.read("discos.csv");

            if(renglones != null){

                //Arreglo de tipo disco
                arrayDiscos = new Disco[renglones.length];

                //Ciclo para generar los objetos de tipo Disco
                for(int i = 0; i < renglones.length; i++){
                    String[] espacios = renglones[i].split(",");
                    
                    //Estructura try-catch anidada, para considerar errores al crear un objeto de tipo Disco
                    try{
                        
                        // Verificación de que existan exactamente 4 datos en el arreglo para poder generar el objeto
                        if (espacios.length != 4) {
                            throw new WrongFormatException();
                        } else {
                            // Paso a entero para obtener un número en la localidad 3 del arreglo
                            int pistas = Integer.parseInt(espacios[3]);

                            // Creación del objeto Disco
                            arrayDiscos[i] = new Disco(espacios[0], espacios[1], espacios[2], pistas);
                        }

                    } catch (NumberFormatException e){ //Excepción en caso de fallar el paso a entero de "n"
                        Colors.println("Aviso: Hay un valor ilegible en el renglón [" + (i+1) + "," + espacios[0] + "]", Colors.RED + Colors.HIGH_INTENSITY);
                        arrayDiscos[i] = new Disco(espacios[0], espacios[1], espacios[2], 0);//Aún así se genera el objeto, pero con "0" como valor del atributo pistas

                    } catch (WrongFormatException e){
                        Colors.println("Aviso: Formato incompatible en la linea [" + (i+1) + "," + espacios[0] + "]", Colors.RED + Colors.HIGH_INTENSITY);
                        arrayDiscos[i] = new Disco("Elemento no leido", "Null", "Null", 0);//Se crea el objeto, pero no guardará ninguna información
                    }
                }

            } else { //Excepción, en caso que no hayan encontrado renglones, significa que no existe el archivo.
                
                throw new IOException();   
        
            }

            return arrayDiscos;

        } catch (IOException e){
            Colors.println("Error, no se pudo leer el archivo :(", Colors.RED + Colors.HIGH_INTENSITY);
            return null;
        }
    }


    /**
     * Método getLibros, genera el arreglo de objetos de tipo Libro tomado del archivo libros.csv
     * @return arrayLibros, el arreglo de objetos de tipo Libro
     */
    public Libro[] arrayLibros() {
        Libro[] arrayLibros = new Libro[0];

        try{
            //Generación de un arreglo empleando ReaderWriter y leyendo el archivo deseado
            String[] renglones = ReaderWriter.read("libros.csv");

            if(renglones != null){

                //Creación del arreglo de libros de la misma longitud que el número de renglones en la base de datos
                arrayLibros = new Libro[renglones.length];

                //Ciclo para generar los objetos de tipo libro
                for(int i = 0; i < renglones.length; i++){
                    
                    //Separación de las cadenas en el arreglo por apariciones de ","
                    String[] espacios = renglones[i].split(",");
                    
                    //Estructura try-catch anidada, para considerar errores al crear un objeto de tipo Película
                    try{
                        
                        //Verificación de que existan exactamente 4 datos en el arreglo para poder generar el objeto
                        if(espacios.length != 4){
                            throw new WrongFormatException();

                        } else{    
                            //Paso a entero para obtener un número en la localidad 3 del arreglo
                            int tema = Integer.parseInt(espacios[3]);
                        
                            //Creación del objeto pelicula
                            arrayLibros[i] = new Libro(espacios[0], espacios[2], tema, espacios[1]);
                        }
                    
                    } catch (NumberFormatException e){ //Excepción de tipo NumberFormatException
                        Colors.println("Aviso: Hay un valor ilegible en la línea [" + (i+1) + "," + espacios[0] + "]", Colors.RED + Colors.HIGH_INTENSITY);
                        arrayLibros[i] = new Libro(espacios[0], espacios[2], 0, espacios[1]);//Aún así se genera el objeto, pero con "0" como valor de "tema"

                    } catch (WrongFormatException e){
                        Colors.println("Aviso: Formato incompatible en la linea [" + (i+1) + "," + espacios[0] + "]", Colors.RED + Colors.HIGH_INTENSITY);
                        arrayLibros[i] = new Libro("Elemento no leido", "Null", 0, "Null");//Se crea el objeto, pero no guardará ninguna información
                    }
                    
                }

            } else { //Excepción, en caso que no hayan encontrado renglones, significa que no existe el archivo.
                throw new IOException();   
            }

            return arrayLibros;//Retorna el arreglo de tipo libro

        } catch (IOException e){//Excepción para lectura del archivo
            Colors.println("Error, no se pudo leer el archivo :(", Colors.RED + Colors.HIGH_INTENSITY);
            return null;
        }
    }

    /**
     * Método getPeliculas, genera el arreglo de objetos de tipo Pelicula tomado del archivo peliculas.csv
     * @return arrayPeliculas, el arreglo de objetos tipo Pelicula
     */
    public Pelicula[] arrayPeliculas() {
        //Definición del arreglo de tipo Película, se inicializa en 0 localidades
        Pelicula[] arrayPeliculas = new Pelicula[0];;

        try{
            //Generación de un arreglo empleando ReaderWriter y leyendo el archivo deseado
            String[] renglones = ReaderWriter.read("peliculas.csv");
            
            if(renglones != null){
                //Arreglo de tipo Pelicula
                arrayPeliculas = new Pelicula[renglones.length];
                
                //Ciclo para generar los objetos de tipo película
                for(int i = 0; i < renglones.length; i++){
                    
                    //Separación de las cadenas en el arreglo por apariciones de ","
                    String[] espacios = renglones[i].split(",");

                    //Estructura try-catch anidada, para considerar errores al crear un objeto de tipo Película
                    try{
                        
                        //Verificación de que existan exactamente 4 datos en el arreglo para poder generar el objeto
                        if(espacios.length != 4){
                            throw new WrongFormatException();

                        } else{
                            //Paso a entero para obtener un número en la localidad 3 del arreglo
                            short year = Short.parseShort(espacios[3]);
                        
                            //Creación del objeto pelicula
                            arrayPeliculas[i] = new Pelicula(espacios[0], espacios[1], espacios[2], year);
                        }

                    } catch (NumberFormatException e){ //Excepción en caso de fallar paso a short de n
                        Colors.println("Aviso: Hay un valor ilegible en la línea [" + (i+1) + "," + espacios[0] + "]", Colors.RED + Colors.HIGH_INTENSITY);
                        short cero = 0;
                        arrayPeliculas[i] = new Pelicula(espacios[0], espacios[1], espacios[2], cero);//Aún así se genera el objeto, pero con "0" como año de filmación

                    } catch (WrongFormatException e){
                        short cero = 0;
                        Colors.println("Aviso: Formato incompatible en la linea [" + (i+1) + "," + espacios[0] + "]", Colors.RED + Colors.HIGH_INTENSITY);
                        arrayPeliculas[i] = new Pelicula("Elemento no leido", "Null", "Null", cero);//Se crea el objeto, pero no guardará ninguna información
                    }
                }

            } else { //Excepción, en caso que no hayan encontrado renglones, significa que no existe el archivo.
                throw new IOException();   
            }

            return arrayPeliculas;//Retorna el arreglo de tipo pelicula

        } catch (IOException e){//Excepción para lectura del archivo
            Colors.println("Error, no se pudo leer el archivo :(", Colors.RED + Colors.HIGH_INTENSITY);
            return null;
        }
    }

}