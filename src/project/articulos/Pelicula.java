package project.articulos;

import project.colors.Colors;

/**
 * Clase Pelicula, se encarga de los objetos de tipo Pelicula.
 */
public class Pelicula extends ArticuloBase{

    /**
     * Atributo año, se encarga de manejar el año de filmación de una película
     */
    private short año;

    /**
     * Constructor de la clase, para generar objetos de tipo Pelicula
     * @param titulo una cadena para registrar el título de la película
     * @param genero cadena para registrar el género de la película
     * @param actor cadena para registrar el nombre del actor/actriz principal
     * @param año número de tipo short para registrar el año de estreno de la película
     */
    public Pelicula(String titulo, String genero, String actor, short año){
        super(titulo, actor, genero);
        this.año = año;
    }

    /**
     * Método "getAño" para solicitar el valor del atributo "año"
     * @return año
     */
    public short getAño(){
        return año;
    }

    /**
     * Método toString, se encarga de generar toda la cadena con el estado de un objeto de tipo Pelicula
     * @return sb, cadena completa.
     */
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        
        //Agregar información de la clase padre
        sb.append(super.toString());
        //Agregar Genero
        sb.append(Colors.HIGH_INTENSITY);
        sb.append("Genero: ");
        sb.append(Colors.RESTORE);
        sb.append(super.getGenero() + "\n");
        sb.append(Colors.RESTORE);
        //Agregar Autor
        sb.append(Colors.HIGH_INTENSITY);
        sb.append("Actor/Actriz: ");
        sb.append(Colors.RESTORE);
        sb.append(super.getArtista() + "\n");
        //Agregar Tema
        sb.append(Colors.HIGH_INTENSITY);//sexo
        sb.append("Año: ");
        sb.append(Colors.RESTORE);
        sb.append(getAño() + "\n");
        sb.append(Colors.RESTORE);
        return sb.toString();
    }
}