package project.articulos;

import project.colors.Colors;

/**
 * Clase Disco, se encarga de los objetos de tipo Disco.
 */
public class Disco extends ArticuloBase{

    /**
     * Atributo pistas, es un entero que almacena el número de pistas de un disco
     */
    private int pistas;

    /**
     * Constructor. Se encarga de generar los objetos de tipo Disco para las instancias de la clase
     * Se apoya del constructor de la clase padre "ArticuloBase"
     * @param titulo una cadena para registrar el título del disco
     * @param genero cadena para registrar el género del disco
     * @param interprete  cadena para registrar el nombre del interprete
     * @param pistas número para recibir el número de pistas del disco
     */
    public Disco(String titulo, String genero, String interprete, int pistas){
    super(titulo, interprete, genero);
    this.pistas = pistas;
    }


    /**
     * Método "getPistas", para solicitar el valor del atributo "pistas"
     * @return pistas
     * 
     */
    public int getPistas(){
        return pistas;
    }

    /**
     * Método toString, se encarga de generar toda la cadena con el estado de un objeto de tipo Disco
     * @return sb, cadena completa.
     */
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        
        //Agregar información de la clase padre
        sb.append(super.toString());
        //Agregar Genero
        sb.append(Colors.HIGH_INTENSITY);
        sb.append("Genero: ");
        sb.append(Colors.RESTORE);
        sb.append(super.getGenero() + "\n");
        sb.append(Colors.RESTORE);
        //Agregar interprete
        sb.append(Colors.HIGH_INTENSITY);
        sb.append("Interprete: ");
        sb.append(Colors.RESTORE);
        sb.append(super.getArtista() + "\n");
        //Agregar Tema
        sb.append(Colors.HIGH_INTENSITY);
        sb.append("Número de pistas: ");
        sb.append(Colors.RESTORE);
        sb.append(getPistas() + "\n");
        sb.append(Colors.RESTORE);
        return sb.toString();
    }
}
