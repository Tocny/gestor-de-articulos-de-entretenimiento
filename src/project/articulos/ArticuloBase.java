package project.articulos;

import project.colors.Colors;

/**
 * Clase ArticuloBase, se encarga de generar la jerarquía de herencia para los distintos articulos en la base de datos
 */
public class ArticuloBase{

    /**
     * Atributos de la clase, serán los atributos que tendrán en común todos los articulos
     */
    protected String titulo, genero, artista;

    /**
     * Constructor de la clase, recibe los tres atributos base de cualquier articulo
     * @param titulo una cadena para registrar el título
     * @param artista cadena para indicar el artista
     * @param genero cadena para indicar el genero
     */
    public ArticuloBase(String titulo, String artista, String genero){
        this.titulo = titulo;
        this.artista = artista;
        this.genero = genero;
    }

    /**
     * Método "getTitulo", para solicitar el valor del atributo "titulo"
     * @return titulo.
     */
    public String getTitulo(){
        return titulo;
    }
    
    /**
     * Método "getArtista", para solicitar el valor del atributo "artista"
     * @return artista.
     */
    public String getArtista(){
        return artista;
    }

    /**
     * Método "getGenero", para solicitar el valor del atributo "genero"
     * @return genero.
     */
    public String getGenero(){
        return genero;
    }

    /**
     * Método toString, genera la cadena del atributo principal, título.
     * @return sb, cadena para el título
     */
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();

        //Agregar título
        sb.append(Colors.HIGH_INTENSITY);
        sb.append("Titulo: ");
        sb.append(Colors.RESTORE);
        sb.append(getTitulo() + "\n");
        
        return sb.toString();

    }

}