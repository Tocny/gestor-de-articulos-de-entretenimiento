package project.articulos;

import project.colors.Colors;

/**
 * Clase Libro, se encarga de los objetos de tipo Libro
 */
public class Libro extends ArticuloBase{

    /**
     * Atributo tema, es un entero que maneja el número de tema indexado
     */
    private int tema;

    /**
     * Constructor de la clase, genera los objetos de tipo Libro a partir de la clase padre
     * @param titulo una cadena para registrar el nombre del libro
     * @param autor cadena para registrar el autor del libro
     * @param tema entero que será el tema del libro
     * @param genero cadena para registrar el género del libro
     */
    public Libro(String titulo, String autor, int tema, String genero){
        super(titulo, autor, genero);
        this.tema = tema;
    }

    /**
     * método "getTema", para solicitar el valor del atributo "Tema"
     * @return tema
     */
    public int getTema(){
        return tema;
    }

    /**
     * Método toString, se encarga de generar toda la cadena con el estado de un objeto de tipo Pelicula
     * @return sb, cadena completa.
     */
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        
        //Agregar información de la clase padre
        sb.append(super.toString());
        //Agregar Autor
        sb.append(Colors.HIGH_INTENSITY);
        sb.append("Autor: ");
        sb.append(Colors.RESTORE);
        sb.append(super.getArtista() + "\n");
        //Agregar Tema
        sb.append(Colors.HIGH_INTENSITY);
        sb.append("Tema: ");
        sb.append(Colors.RESTORE);
        sb.append(getTema() + "\n");
        sb.append(Colors.RESTORE);
        //Agregar Genero
        sb.append(Colors.HIGH_INTENSITY);
        sb.append("Genero: ");
        sb.append(Colors.RESTORE);
        sb.append(super.getGenero() + "\n");
        sb.append(Colors.RESTORE);
        return sb.toString();
    }


}